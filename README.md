# Taka11

An emulator for the [Takalaki stack-machine](http://wastingmoves.com/takalaki.html), written in ANSI C and based on [Uxn11](https://git.sr.ht/~rabbits/uxn11).

## Building 

### Graphical

All you need is X11.

```sh
gcc -Os -DNDEBUG -g0 -s src/laki.c src/devices/system.c src/devices/console.c src/devices/screen.c src/devices/raster.c src/devices/controller.c src/devices/mouse.c src/devices/file.c src/devices/datetime.c src/taka11.c -o bin/taka11 -lX11
```

## Usage

The first parameter is the rom file, the subsequent arguments will be accessible to the rom, via the Console vector.

```sh
bin/taka11 bin/raster.aki arg1 arg2
```

## Devices

The file device is _sandboxed_, meaning that it should not be able to read or write outside of the working directory.

- `00` system
- `10` console(+)
- `20` screen
- `30` raster
- `80` controller
- `90` mouse
- `a0` file
- `c0` datetime

## Emulator Controls

- `F2` toggle on-screen debugger
- `F4` load boot.aki, or reload rom

### Buttons

- `LCTRL` A
- `LALT` B
- `LSHIFT` C 
- `HOME` START
