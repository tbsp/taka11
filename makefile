
CLI_src=src/laki.c src/devices/system.c src/devices/console.c src/devices/file.c src/devices/datetime.c src/devices/mmu.c
EMU_src=${CLI_src} src/devices/screen.c src/devices/raster.c src/devices/controller.c src/devices/mouse.c

RELEASE_flags=-DNDEBUG -O2 -g0 -s
DEBUG_flags=-std=c89 -D_POSIX_C_SOURCE=199309L -DDEBUG -Wall -Wno-unknown-pragmas -Wpedantic -Wshadow -Wextra -Werror=implicit-int -Werror=incompatible-pointer-types -Werror=int-conversion -Wvla -g -Og -fsanitize=address -fsanitize=undefined

.PHONY: all debug dest run test install uninstall format clean archive

all: dest bin/taka11

dest:
	@ mkdir -p bin
run: all bin/taka11
	@ bin/taka11 
test: bin/taka11
	@ ./bin/taka11 -v
install: all bin/taka11
	@ cp bin/taka11 ~/bin/
uninstall:
	@ rm -f ~/bin/taka11
format:
	@ clang-format -i src/taka11.c src/devices/*
clean:
	@ rm -fr bin

bin/taka11: ${EMU_src} src/taka11.c
	@ cc ${RELEASE_flags} ${CFLAGS} ${EMU_src} src/taka11.c -lX11 -lutil -o bin/taka11
