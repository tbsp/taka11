/*
Copyright (c) 2021 Devine Lu Linvega

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

/* clang-format off */

#define PEEK2(d) (*(d) << 8 | (d)[1])
#define POKE2(d, v) { *(d) = (v) >> 8; (d)[1] = (v); }

/* clang-format on */

#define PAGE_PROGRAM 0x0100

typedef unsigned char Uint8;
typedef signed char Sint8;
typedef unsigned short Uint16;
typedef signed short Sint16;
typedef unsigned int Uint32;

typedef struct {
	Uint8 dat[0x100], ptr;
} Stack;

typedef struct Laki {
	Uint8 *ram, dev[0x100];
	Stack wst, rst;
	Uint16 ib;
} Laki;

/* required functions */

extern Uint8 emu_lev(Laki *u, Uint8 addr);
extern void emu_sev(Laki *u, Uint8 addr, Uint8 value);

/* built-ins */

int laki_eval(Laki *u, Uint16 pc, int halt);
