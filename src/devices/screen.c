#include <stdlib.h>
#include <stdio.h>

#include "../laki.h"
#include "screen.h"

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

TakaScreen taka_screen;

/* c = !ch ? color >> 2 : color % 4 + ch == 1 ? 0 : (ch - 2 + (color & 3)) % 3 + 1; */

static Uint8 blending[][16] = {
	{0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3},
	{4, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3},
	{1, 2, 3, 1, 1, 2, 3, 1, 1, 2, 3, 1, 1, 2, 3, 1},
	{2, 3, 1, 2, 2, 3, 1, 2, 2, 3, 1, 2, 2, 3, 1, 2},
	{0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0}};

void
screen_change(Uint16 x1, Uint16 y1, Uint16 x2, Uint16 y2)
{
	if(x1 > taka_screen.width && x2 > x1) return;
	if(y1 > taka_screen.height && y2 > y1) return;
	if(x1 > x2) x1 = 0;
	if(y1 > y2) y1 = 0;
	if(x1 < taka_screen.x1) taka_screen.x1 = x1;
	if(y1 < taka_screen.y1) taka_screen.y1 = y1;
	if(x2 > taka_screen.x2) taka_screen.x2 = x2;
	if(y2 > taka_screen.y2) taka_screen.y2 = y2;
}

void
screen_fill(Uint8 *layer, int color)
{
	int i, length = taka_screen.width * taka_screen.height;
	for(i = 0; i < length; i++)
		layer[i] = color;
}

void
screen_rect(Uint8 *layer, Uint16 x1, Uint16 y1, Uint16 x2, Uint16 y2, int color)
{
	int row, x, y, w = taka_screen.width, h = taka_screen.height;
	for(y = y1; y < y2 && y < h; y++)
		for(x = x1, row = y * w; x < x2 && x < w; x++)
			layer[x + row] = color;
}

static void
screen_2bpp(Uint8 *layer, Uint8 *addr, Uint16 x1, Uint16 y1, Uint16 color, int fx, int fy)
{
	int w = taka_screen.width, h = taka_screen.height, opaque = blending[4][color];
	Uint16 y, ymod = (fy < 0 ? 7 : 0), ymax = y1 + ymod + fy * 8;
	Uint16 x, xmod = (fx > 0 ? 7 : 0), xmax = x1 + xmod - fx * 8;
	for(y = y1 + ymod; y != ymax; y += fy, addr++) {
		int c = addr[0] | (addr[8] << 8), row = y * w;
		if(y < h)
			for(x = x1 + xmod; x != xmax; x -= fx, c >>= 1) {
				Uint8 ch = (c & 1) | ((c >> 7) & 2);
				if(x < w && (opaque || ch))
					layer[x + row] = blending[ch][color];
			}
	}
}

static void
screen_1bpp(Uint8 *layer, Uint8 *addr, Uint16 x1, Uint16 y1, Uint16 color, int fx, int fy)
{
	int w = taka_screen.width, h = taka_screen.height, opaque = blending[4][color];
	Uint16 y, ymod = (fy < 0 ? 7 : 0), ymax = y1 + ymod + fy * 8;
	Uint16 x, xmod = (fx > 0 ? 7 : 0), xmax = x1 + xmod - fx * 8;
	for(y = y1 + ymod; y != ymax; y += fy) {
		int c = *addr++, row = y * w;
		if(y < h)
			for(x = x1 + xmod; x != xmax; x -= fx, c >>= 1) {
				Uint8 ch = c & 1;
				if(x < w && (opaque || ch))
					layer[x + row] = blending[ch][color];
			}
	}
}

/* clang-format off */

static Uint8 icons[] = {
	0x00, 0x7c, 0x82, 0x82, 0x82, 0x82, 0x82, 0x7c, 0x00, 0x30, 0x10, 0x10, 0x10, 0x10, 0x10, 
	0x10, 0x00, 0x7c, 0x82, 0x02, 0x7c, 0x80, 0x80, 0xfe, 0x00, 0x7c, 0x82, 0x02, 0x1c, 0x02, 
	0x82, 0x7c, 0x00, 0x0c, 0x14, 0x24, 0x44, 0x84, 0xfe, 0x04, 0x00, 0xfe, 0x80, 0x80, 0x7c, 
	0x02, 0x82, 0x7c, 0x00, 0x7c, 0x82, 0x80, 0xfc, 0x82, 0x82, 0x7c, 0x00, 0x7c, 0x82, 0x02, 
	0x1e, 0x02, 0x02, 0x02, 0x00, 0x7c, 0x82, 0x82, 0x7c, 0x82, 0x82, 0x7c, 0x00, 0x7c, 0x82, 
	0x82, 0x7e, 0x02, 0x82, 0x7c, 0x00, 0x7c, 0x82, 0x02, 0x7e, 0x82, 0x82, 0x7e, 0x00, 0xfc, 
	0x82, 0x82, 0xfc, 0x82, 0x82, 0xfc, 0x00, 0x7c, 0x82, 0x80, 0x80, 0x80, 0x82, 0x7c, 0x00, 
	0xfc, 0x82, 0x82, 0x82, 0x82, 0x82, 0xfc, 0x00, 0x7c, 0x82, 0x80, 0xf0, 0x80, 0x82, 0x7c,
	0x00, 0x7c, 0x82, 0x80, 0xf0, 0x80, 0x80, 0x80 };
static Uint8 arrow[] = {
	0x00, 0x00, 0x00, 0xfe, 0x7c, 0x38, 0x10, 0x00 };

/* clang-format on */

static void
draw_byte(Uint8 b, Uint16 x, Uint16 y, Uint8 color)
{
	screen_1bpp(taka_screen.fg, &icons[(b >> 4) << 3], x, y, color, 1, 1);
	screen_1bpp(taka_screen.fg, &icons[(b & 0xf) << 3], x + 8, y, color, 1, 1);
	screen_change(x, y, x + 0x10, y + 0x8);
}

static void
screen_debugger(Laki *u)
{
	int i;
	for(i = 0; i < 0x08; i++) {
		Uint8 pos = u->wst.ptr - 4 + i;
		Uint8 color = i > 4 ? 0x01 : !pos ? 0xc
			: i == 4                      ? 0x8
										  : 0x2;
		draw_byte(u->wst.dat[pos], i * 0x18 + 0x8, taka_screen.height - 0x18, color);
	}
	for(i = 0; i < 0x08; i++) {
		Uint8 pos = u->rst.ptr - 4 + i;
		Uint8 color = i > 4 ? 0x01 : !pos ? 0xc
			: i == 4                      ? 0x8
										  : 0x2;
		draw_byte(u->rst.dat[pos], i * 0x18 + 0x8, taka_screen.height - 0x10, color);
	}
	screen_1bpp(taka_screen.fg, &arrow[0], 0x68, taka_screen.height - 0x20, 3, 1, 1);
	for(i = 0; i < 0x20; i++)
		draw_byte(u->ram[i], (i & 0x7) * 0x18 + 0x8, ((i >> 3) << 3) + 0x8, 1 + !!u->ram[i]);
}

void
screen_resize(Uint16 width, Uint16 height, int scale)
{
	Uint8 *bg, *fg;
	Uint32 *pixels = NULL;
	int dim_change = taka_screen.width != width || taka_screen.height != height;
	if(width < 0x8 || height < 0x8 || width >= 0x800 || height >= 0x800 || scale < 1 || scale >= 4)
		return;
	if(taka_screen.width == width && taka_screen.height == height && taka_screen.scale == scale)
		return;
	if(dim_change) {
		bg = malloc(width * height), fg = malloc(width * height);
		if(bg && fg)
			pixels = realloc(taka_screen.pixels, width * height * sizeof(Uint32) * scale * scale);
		if(!bg || !fg || !pixels) {
			free(bg), free(fg);
			return;
		}
		free(taka_screen.bg), free(taka_screen.fg);
	} else {
		bg = taka_screen.bg, fg = taka_screen.fg;
		pixels = realloc(taka_screen.pixels, width * height * sizeof(Uint32) * scale * scale);
		if(!pixels)
			return;
	}
	taka_screen.bg = bg, taka_screen.fg = fg;
	taka_screen.pixels = pixels;
	taka_screen.width = width, taka_screen.height = height, taka_screen.scale = scale;
	if(dim_change)
		screen_fill(taka_screen.bg, 0x4), screen_fill(taka_screen.fg, 0x4);
	emu_resize(width, height);
	screen_change(0, 0, width, height);
}

void
screen_redraw(Laki *u)
{
	int i, x, y, k, l, s = taka_screen.scale;
	Uint8 *fg = taka_screen.fg, *bg = taka_screen.bg;
	Uint16 w = taka_screen.width, h = taka_screen.height;
	Uint16 x1 = taka_screen.x1, y1 = taka_screen.y1;
	Uint16 x2 = taka_screen.x2 > w ? w : taka_screen.x2, y2 = taka_screen.y2 > h ? h : taka_screen.y2;
	Uint32 palette[32], *pixels = taka_screen.pixels;
	taka_screen.x1 = taka_screen.y1 = 0xffff;
	taka_screen.x2 = taka_screen.y2 = 0;
	if(u->dev[0x0e])
		screen_debugger(u);

	if((u->dev[0x26] & 0x1f) == 0x3) {
		/* When only fg/bg layers are active use simplified redraw */
		for(i = 0; i < 32; i++)
			palette[i] = taka_screen.palette[(i >> 4) ? (i & 3) : (i >> 2)];
		for(y = y1; y < y2; y++) {
			int ys = y * s, o = y * w;
			/*for(o = y * w, i = x1 + o, j = x2 + o; i < j; i++)*/
			for(x = x1, i = x1 + o; x < x2; x++, i++) {
				int c = palette[(fg[i] << 2) | (bg[i] & 0x3)];
				for(k = 0; k < s; k++) {
					int oo = ((ys + k) * w + x) * s;
					for(l = 0; l < s; l++)
					pixels[oo + l] = c;
				}
			}
		}
	} else {
		Uint32 pxColor;
		Uint16 xOffset, yOffset, addr, raster_vector, match;
		Uint8 j, tile, attr, blend, priority, twobpp, xflip, yflip, color, opaque, enabled, winInVert, enabledLocal;
		Uint8 *layer;
		TileLayer *tileLayer;

		for(y = 0, i = 0; y < taka_screen.height; y++) {
			/* Evaluate raster vector if active */
			POKE2(&u->dev[0x36], y);
			raster_vector = PEEK2(&u->dev[0x30]);
			match = PEEK2(&u->dev[0x34]);
			if(raster_vector && (!match || (match == y))) laki_eval(u, raster_vector, 1);

			enabled = u->dev[0x26] & 0xf;
			winInVert = y >= taka_screen.winEdge[2] && y < taka_screen.winEdge[3];

			int ys = y * s;
			for(x = 0; x < taka_screen.width; x++) {
				/* Determine active layers for pixel */
				if(u->dev[0x26] & 0x10)
					if(winInVert && (x >= taka_screen.winEdge[0] && x < taka_screen.winEdge[1])) enabledLocal = u->dev[0x38] & enabled;
					else enabledLocal = u->dev[0x38] >> 4 & enabled;
				else enabledLocal = enabled;

				/* Determine color of pixel based on active layers, in pairs */
				pxColor = 0;
				for(j = 0, tileLayer = taka_screen.tile; j < 2; j++, tileLayer++ ) {
					if(enabledLocal & (4 << j)) {
						xOffset = (x + tileLayer->scrollX) & ((tileLayer->width << 3) - 1);
						yOffset = (y + tileLayer->scrollY) & ((tileLayer->height << 3) - 1);
						addr = tileLayer->mapAddr + ((yOffset >> 3) * tileLayer->width + (xOffset >> 3));
						tile = u->ram[addr];
						attr = tileLayer->attr;
						if(tileLayer->attrMap) attr ^= u->ram[addr + tileLayer->size];
						blend = attr & 0x0f;
						xflip = attr & 0x10;
						yflip = attr & 0x20;
						priority = attr & 0x40;
						twobpp = !!(attr & 0x80);
						yOffset ^= yflip ? 7 : 0;
						Uint8 *tileData = &u->ram[tileLayer->tileAddr + (tile << 3) * (twobpp + 1) + yOffset % 8];
						xOffset = (xOffset ^ (xflip ? 0 : 7)) & 7;
						color = (tileData[0] >> xOffset) % 2 | (twobpp ? (tileData[8] >> xOffset) % 2 << 1 : 0);
						opaque = (blend % 5) || !!color;
						if(opaque) pxColor = blending[color][blend];
					} else opaque = 0;
					if(!opaque || (opaque && !priority)) {
						layer = j ? taka_screen.fg : taka_screen.bg;
						if(enabledLocal & (1 << j) && layer[i] != 0x4) pxColor = layer[i];
					}
				}

				int c = taka_screen.palette[pxColor & 0x3];
				for(k = 0; k < s; k++) {
					int oo = ((ys + k) * w + x) * s;
					for(l = 0; l < s; l++)
						pixels[oo + l] = c;
				}
				i++;
			}
		}
	}
}

/* screen registers */

static Uint16 rX, rY, rA, rMX, rMY, rMA, rML, rDX, rDY;

Uint8
screen_lev(Laki *u, Uint8 addr)
{
	switch(addr) {
	case 0x22: return taka_screen.width >> 8;
	case 0x23: return taka_screen.width;
	case 0x24: return taka_screen.height >> 8;
	case 0x25: return taka_screen.height;
	case 0x28: return rX >> 8;
	case 0x29: return rX;
	case 0x2a: return rY >> 8;
	case 0x2b: return rY;
	case 0x2c: return rA >> 8;
	case 0x2d: return rA;
	default: return u->dev[addr];
	}
}

void
screen_sev(Uint8 *ram, Uint8 *d, Uint8 port)
{
	switch(port) {
	case 0x3: screen_resize(PEEK2(d + 2), taka_screen.height, taka_screen.scale); return;
	case 0x5: screen_resize(taka_screen.width, PEEK2(d + 4), taka_screen.scale); return;
	case 0x7: rMX = d[0x7] & 0x20, rMY = d[0x7] & 0x40, rMA = d[0x7] & 0x80, rML = d[0x7] & 0x1f, rDX = rMX >> 2, rDY = rMY >> 3; return;
	case 0x8:
	case 0x9: rX = (d[0x8] << 8) | d[0x9]; return;
	case 0xa:
	case 0xb: rY = (d[0xa] << 8) | d[0xb]; return;
	case 0xc:
	case 0xd: rA = (d[0xc] << 8) | d[0xd]; return;
	case 0xe: {
		Uint8 ctrl = d[0xe];
		Uint8 color = ctrl & 0x04 ? 4 : ctrl & 0x3;
		Uint8 *layer = ctrl & 0x40 ? taka_screen.fg : taka_screen.bg;
		/* fill mode */
		if(ctrl & 0x80) {
			Uint16 x1, y1, x2, y2;
			if(ctrl & 0x8) /* window fill */
				x1 = taka_screen.winEdge[0], x2 = taka_screen.winEdge[1], y1 = taka_screen.winEdge[2], y2 = taka_screen.winEdge[3];
			else { /* quadrant fill */
				if(ctrl & 0x10)
					x1 = 0, x2 = rX;
				else
					x1 = rX, x2 = taka_screen.width;
				if(ctrl & 0x20)
					y1 = 0, y2 = rY;
				else
					y1 = rY, y2 = taka_screen.height;
			}
			screen_rect(layer, x1, y1, x2, y2, color);
			screen_change(x1, y1, x2, y2);
		}
		/* pixel mode */
		else {
			Uint16 w = taka_screen.width;
			if(rX < w && rY < taka_screen.height)
				layer[rX + rY * w] = color;
			screen_change(rX, rY, rX + 1, rY + 1);
			if(rMX) rX++;
			if(rMY) rY++;
		}
		return;
	}
	case 0xf: {
		Uint8 i;
		Uint8 ctrl = d[0xf];
		Uint8 twobpp = !!(ctrl & 0x80);
		Uint8 color = ctrl & 0xf;
		Uint8 *layer = ctrl & 0x40 ? taka_screen.fg : taka_screen.bg;
		int fx = ctrl & 0x10 ? -1 : 1;
		int fy = ctrl & 0x20 ? -1 : 1;
		Uint16 dxy = rDX * fy, dyx = rDY * fx, addr_incr = rMA >> (4 - twobpp);
		if(twobpp)
			for(i = 0; i <= rML; i++, rA += addr_incr)
				screen_2bpp(layer, &ram[rA], rX + dyx * i, rY + dxy * i, color, fx, fy);
		else
			for(i = 0; i <= rML; i++, rA += addr_incr)
				screen_1bpp(layer, &ram[rA], rX + dyx * i, rY + dxy * i, color, fx, fy);
		screen_change(rX, rY, rX + dyx * rML + 8, rY + dxy * rML + 8);
		if(rMX) rX += rDX * fx;
		if(rMY) rY += rDY * fy;
		return;
	}
	}
}
