/*
Copyright (c) 2022 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define SYSTEM_VERSION 2

extern char *boot_rom;

int system_error(char *msg, const char *err);
void system_reboot(Laki *u, char *rom, int soft);
void system_inspect(Laki *u, Uint16 pc);
int system_init(Laki *u, Uint8 *ram, char *rom);

Uint8 system_lev(Laki *u, Uint8 addr);
void system_sev(Laki *u, Uint8 *d, Uint8 port);
