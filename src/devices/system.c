#include <stdio.h>
#include <stdlib.h>

#include "../laki.h"
#include "system.h"
#include "mmu.h"

/*
Copyright (c) 2022-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

char *boot_rom;
Uint16 dev_vers[0x10];

static int
system_load(Laki *u, char *filename)
{
	int l, i = 0;
	FILE *f = fopen(filename, "rb");
	if(!f)
		return 0;
	l = fread(&u->ram[PAGE_PROGRAM], 0x10000 - PAGE_PROGRAM, 1, f);
	while(l && ++i < RAM_PAGES)
		l = fread(u->ram + 0x10000 * i, 0x10000, 1, f);
	fclose(f);
	return 1;
}

static void
system_print(Stack *s)
{
	Uint8 i;
	for(i = s->ptr - 8; i != (Uint8)(s->ptr); i++)
		fprintf(stderr, "%02x%c", s->dat[i], i == 0xff ? '|' : ' ');
	fprintf(stderr, "< \n");
}

static void
system_zero(Laki *u, int soft)
{
	int i;
	for(i = PAGE_PROGRAM * soft; i < 0x10000; i++)
		u->ram[i] = 0;
	for(i = 0x0; i < 0x100; i++)
		u->dev[i] = 0;
	u->wst.ptr = u->rst.ptr = 0;
}

void
system_inspect(Laki *u, Uint16 pc)
{
	fprintf(stderr, "WST "), system_print(&u->wst);
	fprintf(stderr, "RST "), system_print(&u->rst);
}

int
system_error(char *msg, const char *err)
{
	fprintf(stderr, "%s %s\n", msg, err);
	fflush(stderr);
	return 0;
}

void
system_reboot(Laki *u, char *rom, int soft)
{
	system_zero(u, soft);
	if(system_load(u, boot_rom))
		if(laki_eval(u, PAGE_PROGRAM, 1))
			boot_rom = rom;
}

int
system_init(Laki *u, Uint8 *ram, char *rom)
{
	u->ram = ram;
	system_zero(u, 0);
	if(!system_load(u, rom))
		if(!system_load(u, "boot.aki"))
			return system_error("Init", "Failed to load rom.");
	boot_rom = rom;
	return 1;
}

/* IO */

Uint8
system_lev(Laki *u, Uint8 addr)
{
	switch(addr) {
	case 0x4: return u->wst.ptr;
	case 0x5: return u->rst.ptr;
	default: return u->dev[addr];
	}
}

void
system_sev(Laki *u, Uint8 *d, Uint8 port)
{
	Uint8 *ram;
	Uint16 addr;
	switch(port) {
	case 0x3:
		ram = u->ram;
		addr = PEEK2(d + 2);
		fprintf(stderr, "Unknown Expansion Command 0x%02x\n", ram[addr]);
		break;
	case 0xe:
		system_inspect(u, 0);
		break;
	}
}
