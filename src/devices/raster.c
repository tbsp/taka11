#include <stdlib.h>
#include <stdio.h>

#include "../laki.h"
#include "screen.h"
#include "raster.h"

/*
Copyright (c) 2023 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

/* IO */

void
raster_sev(Uint8 *d, Uint8 port)
{
    int i;
    Uint16 x, y;
	switch(port) {
	case 0x3: {
		Uint8 i = d[0x3] & 0x3;
		Uint8
			r = (d[0x2] >> 4) & 0xf,
			g = (d[0x2]) & 0xf,
			b = (d[0x3] >> 4) & 0xf;
		if(d[0x3] & 0x4) {
			Uint8
				cr = taka_screen.palette[i] & 0x000f0000 >> 16,
				cg = taka_screen.palette[i] & 0x00000f00 >> 8,
				cb = taka_screen.palette[i] & 0x0000000f;
			r = cr > r ? cr-1 : (cr < r ? cr+1 : r);
			g = cg > g ? cg-1 : (cg < g ? cg+1 : g);
			b = cb > b ? cb-1 : (cb < b ? cb+1 : b);
		}
		taka_screen.palette[i] = 0x0f000000 | r << 16 | g << 8 | b;
		taka_screen.palette[i] |= taka_screen.palette[i] << 4;
		screen_change(0, 0, taka_screen.width, taka_screen.height);
		return;
	}
    case 0x9:
        x = PEEK2(d - 0x30 + 0x28), y = PEEK2(d - 0x30 + 0x2a);
        if(d[0x9] & 0x01) taka_screen.tile[0].scrollX = x;
        if(d[0x9] & 0x02) taka_screen.tile[0].scrollY = y;
        if(d[0x9] & 0x04) taka_screen.tile[1].scrollX = x;
        if(d[0x9] & 0x08) taka_screen.tile[1].scrollY = y;
        if(d[0x9] & 0x10) taka_screen.winEdge[0] = x;
        if(d[0x9] & 0x20) taka_screen.winEdge[1] = x;
        if(d[0x9] & 0x40) taka_screen.winEdge[2] = y;
        if(d[0x9] & 0x80) taka_screen.winEdge[3] = y;
        break;
	case 0xa:
        i = !!(d[0xa] & 0x10);
        if(d[0xa] & 0x20) {
            taka_screen.tile[i].mapAddr = PEEK2(d - 0x30 + 0x2c);
        }
        taka_screen.tile[i].width = 16 << (d[0xa] & 0x03);
        taka_screen.tile[i].height = 16 << ((d[0xa] & 0x0c) >> 2);
        taka_screen.tile[i].attrMap = !!(d[0xa] & 0x40);
        taka_screen.tile[i].size = taka_screen.tile[i].width * taka_screen.tile[i].height;
		break;
	case 0xb:
        i = !!(d[0xb] & 0x10);
        if(d[0xb] & 0x20) {
            taka_screen.tile[i].tileAddr = PEEK2(d - 0x30 + 0x2c);
        }
        taka_screen.tile[i].attr = (d[0xb] & 0xcf);
		break;
	}
}
