#include "../laki.h"
#include "mouse.h"

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

void
mouse_down(Laki *u, Uint8 *d, Uint8 mask)
{
	d[6] |= mask;
	laki_eval(u, PEEK2(d), 1);
}

void
mouse_up(Laki *u, Uint8 *d, Uint8 mask)
{
	d[6] &= (~mask);
	laki_eval(u, PEEK2(d), 1);
}

void
mouse_pos(Laki *u, Uint8 *d, Uint16 x, Uint16 y)
{
	*(d + 2) = x >> 8, *(d + 3) = x;
	*(d + 4) = y >> 8, *(d + 5) = y;
	laki_eval(u, PEEK2(d), 1);
}

void
mouse_scroll(Laki *u, Uint8 *d, Uint16 x, Uint16 y)
{
	*(d + 0xa) = x >> 8, *(d + 0xb) = x;
	*(d + 0xc) = -y >> 8, *(d + 0xd) = -y;
	laki_eval(u, PEEK2(d), 1);
	*(d + 0xa) = *(d + 0xb) = *(d + 0xc) = *(d + 0xd) = 0;
}
